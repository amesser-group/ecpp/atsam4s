/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of ECPP ATSAM package
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *ElementCnt
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ecpp/HAL/ATSAM/SSC.hpp"
#include "ecpp/HAL/ATSAM/PMC.hpp"
#include "array"

using namespace ecpp::HAL::ATSAM;

void SyncSerialController::SetupI2SSlave(unsigned int bits)
{
  PowerManagementController::EnableClock(SyncSerialController0Trait());  

  SSC->SSC_CR   = SSC_CR_SWRST;

  SSC->SSC_CMR  = 0;
  SSC->SSC_RCMR = SSC_RCMR_CKS_RK | SSC_RCMR_CKO_NONE 
                /* | SSC_RCMR_CKI */
                | SSC_RCMR_CKG_CONTINUOUS
                | SSC_RCMR_START_RF_EDGE
                | (1 << SSC_RCMR_STTDLY_Pos);

  SSC->SSC_RFMR = ((bits - 1) << SSC_RFMR_DATLEN_Pos)
                | SSC_RFMR_MSBF
                | SSC_RFMR_FSOS_NONE
                | SSC_RFMR_FSEDGE_POSITIVE
                | (1 << SSC_RFMR_DATNB_Pos);

  SSC->SSC_TCMR = SSC_TCMR_CKS_RK | SSC_TCMR_CKO_NONE
                /* | SSC_TCMR_CKI */
                | SSC_TCMR_CKG_CONTINUOUS   
                | SSC_TCMR_START_RECEIVE
                | (1 << SSC_TCMR_STTDLY_Pos);  

  SSC->SSC_TFMR = ((bits - 1) << SSC_TFMR_DATLEN_Pos)
                | SSC_TFMR_MSBF
                | SSC_TFMR_FSOS_NONE
                | SSC_TFMR_FSEDGE_POSITIVE
                | (1 << SSC_TFMR_DATNB_Pos);

  SSC->SSC_IDR  = 0xFFFFFFFF; 
  
}

static ::std::array<int16_t[2], 109> wavetable;


void SyncSerialController::Start()
{
  auto it = wavetable.begin();

  for(; it < (wavetable.begin() + wavetable.size()/2); ++it)
  {
    (*it)[0]= 10000;
    (*it)[1]= 10000;
  }

  for(; it < wavetable.end(); ++it)
  {
    (*it)[0]= -10000;
    (*it)[1]= -10000;
  }

  SSC->SSC_CR   = SSC_CR_RXEN | SSC_CR_TXEN;
  SSC->SSC_PTCR = SSC_PTCR_TXTEN;

  SSC->SSC_IER  = SSC_IER_ENDTX | SSC_IER_TXBUFE;
}

volatile unsigned long sample_count = 0;

void SSC_Handler()
{
  uint32_t ssc_sr = SSC->SSC_SR;

  if (ssc_sr & SSC_SR_TXBUFE)
  {
    SSC->SSC_TPR = reinterpret_cast<uintptr_t>(wavetable.data());
    SSC->SSC_TCR  = wavetable.size() * 2;
  }

  if (ssc_sr & (SSC_SR_ENDTX | SSC_SR_TXBUFE))
  {
    SSC->SSC_TNPR = reinterpret_cast<uintptr_t>(wavetable.data());
    SSC->SSC_TNCR  = wavetable.size() * 2;

    sample_count += wavetable.size();
  }
}


