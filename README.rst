######################################
ECPP Atmel/Microchip ATSAM Integration
######################################

This module provides platform support for Atmel/Microchip ATSAM 
microcontrollers. It ships with the cmsis headers retrieved
from Atmel/Microchip.

Copyright & License
===================

* files in subfolder ``atmel-cmsis`` are taken from Atmel/Microchip
  [ASF]_ The license conditions are given in the files itself.

* files in subfolder ``atmel-asf`` are taken from Atmel/Microchip
  [ASF]_ The license conditions are given in the files itself.


References
=========

.. [ASF] https://asf.microchip.com/docs/latest/