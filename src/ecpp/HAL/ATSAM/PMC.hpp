/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of ECPP ATSAM package
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECPP_HAL_ATMEL_SAM_PMC_H_
#define ECPP_HAL_ATMEL_SAM_PMC_H_

#include "sam4s.h"

namespace ecpp::HAL::ATSAM
{
  namespace PowerManagementController
  {
    template<typename PeripheralTrait>
    void EnableClock(PeripheralTrait trait)
    {
      unsigned int id = trait.kPeripheralId;

    	if (id < 32) 
  			PMC->PMC_PCER0 = 1 << id;
#if (SAM3S || SAM3XA || SAM4S || SAM4E || SAM4C || SAM4CM || SAM4CP || SAMG55 || SAMV71 || SAMV70 || SAME70 || SAMS70)
	    else
  			PMC->PMC_PCER1 = 1 << (id - 32);
#endif
	  }

  }
}
#endif