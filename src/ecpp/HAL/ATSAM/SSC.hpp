/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of ECPP ATSAM package
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECPP_HAL_ATSAM_SSC_H_
#define ECPP_HAL_ATSAM_SSC_H_

#include "sam4s.h"

namespace ecpp::HAL::ATSAM
{
  class SyncSerialController0Trait
  {
  public:
    static constexpr unsigned int kPeripheralId = ID_SSC;
  };

  class SyncSerialController
  {
  public:
    static void Start();
    static void SetupI2SSlave(unsigned int bits);
  };

};

#endif
