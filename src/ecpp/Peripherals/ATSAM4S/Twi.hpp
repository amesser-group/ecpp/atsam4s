/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_PERIPHERALS_ATSAM4S_TWI_HPP_
#define ECPP_PERIPHERALS_ATSAM4S_TWI_HPP_

#include "ecpp/BinaryStruct.hpp"
#include "Platform.hpp"
#include <cstdint>
#include <cstdlib>

namespace ecpp::Peripherals::ATSAM4S
{
  using ::std::size_t;
  using ::ecpp::BinaryStruct;

  class Twi
  {
  protected:
    class RequestBlockBase
    {
    public:
      enum class State : uint8_t
      {
        Idle,
        Queued,
        Pending,
        Success,
        Error
      };

    public:
      void SetUp(uint8_t addr, void* dma_buffer) 
      { 
        state_      = State::Idle;
        addr_       = addr; 
        dma_buffer_ = dma_buffer;
        write_len_  = 0;
        read_len_   = 0;
      }

      constexpr uint_least16_t GetReadLen()  const   { return read_len_;  }
      constexpr uint_least16_t GetWriteLen() const   { return write_len_; }

      bool                     IsActive()      const { return (state_ == State::Queued) || (state_ == State::Pending); }
      bool                     WasSuccessful() const { return state_ == State::Success; }
    protected:
      volatile State     state_;
      uint_least8_t      addr_;
      void              *dma_buffer_;
      uint_least16_t     write_len_;
      uint_least16_t     read_len_;

      friend class Twi;
    };

    void HandleRequest(RequestBlockBase *req);
  public:
    void Initialize();


    template<uint_least16_t BUFFERLEN, typename Base = RequestBlockBase>
    class RequestBlock : public Base
    {
    public:
      typedef uint8_t Buffer[BUFFERLEN];

      void SetUp(uint8_t addr) { Base::SetUp(addr, buffer_); }

      using Base::GetReadLen;
      using Base::GetWriteLen;
      using Base::IsActive;
      using Base::WasSuccessful;

      auto & GetBuffer()                    { return buffer_; }
      constexpr size_t GetBufferSize()      { return sizeof(buffer_); }

      constexpr BinaryStruct   GetStruct()  { return {buffer_, sizeof(Buffer)}; }
    protected:
      Buffer   buffer_;
    };
  };

}



#endif /* ECPP_PERIPHERALS_ATSAM4S_TWI_HPP_ */
