/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Peripherals/ATSAM4S/Twi.hpp"
#include "System.hpp"
#include "ecpp/Container.hpp"

namespace asf
{
#include "compiler.h"
#include "conf_clock.h"
#include "sysclk.h"
#include "ioport.h"
#include "pio.h"
#include "twi_master.h"

static Twi* const Twi0 = TWI0;
}

using namespace ecpp::Peripherals;

void
ATSAM4S::Twi::HandleRequest(ATSAM4S::Twi::RequestBlockBase *req)
{
  auto p_twi = asf::Twi0;
  uint32_t mask_sr_finish;
  bool success = false;

  if(req->read_len_ > 0)
  {
    if(req->write_len_ <= 3)
    {
      req->state_ = req->State::Pending;

      /* Set read mode, slave address and 3 internal address byte lengths */
      p_twi->TWI_MMR = TWI_MMR_MREAD | TWI_MMR_DADR(req->addr_) |
          ((req->write_len_ << TWI_MMR_IADRSZ_Pos) &  TWI_MMR_IADRSZ_Msk);

      /* Set internal address for remote chip */
      p_twi->TWI_IADR = 0;
      p_twi->TWI_IADR = asf::twi_mk_addr(reinterpret_cast<uint8_t*>(req->dma_buffer_), req->write_len_);

      p_twi->TWI_RPR  = reinterpret_cast<uintptr_t>(req->dma_buffer_);
      p_twi->TWI_RCR  = req->read_len_;

      p_twi->TWI_PTCR = TWI_PTCR_RXTEN;
      p_twi->TWI_CR   = TWI_CR_START;
    }
    else
    {
      req->state_ = req->State::Error;
    }

    mask_sr_finish  = TWI_SR_ENDRX;
  }
  else
  {
    req->state_ = req->State::Pending;

    p_twi->TWI_MMR = TWI_MMR_DADR(req->addr_);

    p_twi->TWI_TPR  = (uintptr_t)req->dma_buffer_;
    p_twi->TWI_TCR  = req->write_len_;

    p_twi->TWI_CR   = TWI_CR_START;
    p_twi->TWI_PTCR = TWI_PTCR_TXTEN;

    mask_sr_finish  = TWI_SR_ENDTX;
  }

  if(req->state_ == req->State::Pending)
  {
    uint32_t sr_reg;
    /* wait for transfer to finish */
    success = true;
    do
    {
      sr_reg = p_twi->TWI_SR;

      if (sr_reg & TWI_SR_NACK)
      {
        success = false;
        break;
      }
    } while((sr_reg & mask_sr_finish) == 0);

    p_twi->TWI_CR   = TWI_CR_STOP;
    p_twi->TWI_PTCR = (TWI_PTCR_RXTDIS | TWI_PTCR_TXTDIS);

    while (!(p_twi->TWI_SR & TWI_SR_TXCOMP)) {}
    p_twi->TWI_SR;
    p_twi->TWI_RHR; /* clear rx transfer reg */

  }
  
  if(success)
  {
    req->state_ = req->State::Success;
  }
  else
  {
    req->state_ = req->State::Error;

    req->write_len_ = 0;
    req->read_len_ = 0;
  }
}


void 
ATSAM4S::Twi::Initialize()
{
  asf::twi_master_options_t opt = {0};

  opt.speed = 200000;
  opt.chip  = 0xff;

  // Initialize the TWI master driver.

  asf::twi_master_enable(asf::Twi0);
  asf::twi_master_setup(asf::Twi0, &opt);

  /* Initialize the TWIM Module */
}

