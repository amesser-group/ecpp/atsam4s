#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et

def configure(conf):

    atmel_cmsis_folder = {
        'atsam4s8b' : 'atmel-cmsis/sam4s/include',
    } [conf.env.DEVICE]


    f = conf.env.append_value

    f('INCLUDES', [ 
        conf.path.find_dir(atmel_cmsis_folder).abspath(),
        conf.path.find_dir('atmel-asf').abspath(),
        conf.path.find_dir('src').abspath(),
    ])

def build(bld):
    bld.ecpp_build(
        target   = 'ecpp-atsam_' + bld.env.ECPP_BUILDID,
        source   = bld.path.find_dir('src').ant_glob('**/*.cpp') +
                   bld.path.find_dir('atmel-asf').ant_glob('**/*.c'),
        features = 'cxx cstlib',
    )
